import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import './index.css';

import SignIn from './SignIn';
import Walkthrough from './Walkthrough';

ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/" component={SignIn}/>
            <Route exact path="/walkthrough" component={Walkthrough}/>
            <Redirect to="/"/>
        </Switch>
    </Router>,
    document.getElementById('root')
);
