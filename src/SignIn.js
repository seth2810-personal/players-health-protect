import React from 'react';
import { withRouter } from 'react-router-dom';

import Page from './shared/components/Page';
import Header from './shared/components/Header';
import Content from './shared/components/Content';
import buttonStyles from './shared/components/Button.module.css';

import SignInForm from './SignInForm';

const SignIn = ({ history }) => (
  <Page>
    <Header>
      <button className={`${buttonStyles.button} ${buttonStyles.buttonGhost}`} type="button">
        Sign up
      </button>
    </Header>
    <Content>
      <h1>Sign in</h1>
      <SignInForm onSubmit={() => history.push('/walkthrough')}/>
    </Content>
  </Page>
);

export default withRouter(SignIn);