import { useState, useEffect } from 'react';

const useForm = (validate, callback) => {
    const [values, setValues] = useState({});
    const [errors, setErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
            callback();
        }
        
        setIsSubmitting(false);
    }, [errors]);

    const handleSubmit = (event) => {
        event.preventDefault();
        setIsSubmitting(true);
        setErrors(validate(values));
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setValues({ ...values, [name]: value });
    };

    return { values, errors, handleChange, handleSubmit };
};

export default useForm;