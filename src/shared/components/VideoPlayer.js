import React from 'react';

import styles from './VideoPlayer.module.css';

export default class VideoPlayer extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = { play: false };
        this.playerRef = React.createRef();
    }

    playVideo = () => {
        this.playerRef.current.play();
        this.setState({ play: true });
    }

    togglePlay = () => {
        this.setState({ play: false });
    }

    componentDidMount() {
        this.playerRef.current.addEventListener('ended', this.togglePlay);
    }

    componentWillUnmount() {
        this.playerRef.current.removeEventListener('ended', this.togglePlay);
    }

    render() {
        return (
            <div className={styles.container}>
                <video src={this.props.src} ref={this.playerRef}/>
                <div className={styles.buttonContainer} style={{ opacity: this.state.play ? 0 : 1 }}>
                    <button className={styles.button} type="button" onClick={this.playVideo}></button>
                </div>
            </div>
        );
    }
}
