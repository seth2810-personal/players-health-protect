import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Header.module.css';

const Header = ({ children }) => (
    <header className={styles.header}>
        <Link className={styles.logo} to="/">
            <img src="/images/logo.svg" alt="Player's Health Protect"/>
        </Link>
        {children}
    </header>
);

export default Header;