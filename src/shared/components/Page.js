import React from 'react';

import styles from './Page.module.css';

const Page = ({ children }) => (
    <section className={styles.page}>
        <div className={styles.background}>
            <div className={styles.backgroundGradient}></div>
            <div className={styles.backgroundCircleTop}></div>
            <div className={styles.backgroundCircleBottom}></div>
        </div>
        <section className={styles.content}>
            {children}
        </section>
    </section>
);

export default Page;