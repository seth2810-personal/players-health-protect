import React from 'react';
import * as validator from 'validator';
import { Link } from 'react-router-dom';

import useForm from './shared/hooks/useForm';
import buttonStyles from './shared/components/Button.module.css';

import styles from './SignInForm.module.css';

const validate = (values) => {
    const errors = {};
    const { username = '', password = '' } = values;

    if (validator.isEmpty(username)) {
        errors.username = 'Username is required';
    } else if (validator.isEmail(username)) {
        // skip
    } else if (validator.isMobilePhone(username)) {
        // skip
    } else {
        errors.username = 'Username is not email or phone number';
    }

    if (validator.isEmpty(password)) {
        errors.password = 'Password is required';
    }

    return errors;
};

const SignInForm = ({ onSubmit }) => {
    const { values, errors, handleChange, handleSubmit } = useForm(validate, onSubmit);

    return (
        <form className={styles.form} onSubmit={handleSubmit}>
            <div className={styles.fields}>
                <div className={styles.field}>
                    <input id="username" name="username" value={values.username || ''} onChange={handleChange}
                        type="text" placeholder="Email / phone number"/>
                    <label htmlFor="username">Email / phone number</label>
                    {errors.username && (
                        <div className={styles.error}>{errors.username}</div>
                    )}
                </div>
                <div className={styles.field}>
                    <input id="password" name="password" value={values.password || ''} onChange={handleChange}
                        type="password" placeholder="Password" autoComplete="false"/>
                    <label htmlFor="password">Password</label>
                    {errors.password && (
                        <div className={styles.error}>{errors.password}</div>
                    )}
                </div>
            </div>
            <Link to="/reset_password">Reset password</Link>
            <div className={styles.buttons}>
                <button className={`${buttonStyles.button} ${buttonStyles.buttonSuccess}`}
                    type="submit">
                    Sign in
                </button>
                <div className={styles.textWithDivider}>Or</div>
                <button className={`${buttonStyles.button} ${buttonStyles.buttonGhost} ${buttonStyles.buttonSuccess} ${styles.seButton}`}
                    type="button">
                    Sign in with
                    <img className={styles.seIcon} src="/images/se-icon.svg" alt="Sports Engine"/>
                </button>
            </div>
        </form>
    );
};

export default SignInForm;