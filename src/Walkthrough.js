import React from 'react';
import { withRouter } from 'react-router-dom';

import Page from './shared/components/Page';
import Header from './shared/components/Header';
import Content from './shared/components/Content';
import VideoPlayer from './shared/components/VideoPlayer';

import styles from './Walkthrough.module.css';
import buttonStyles from './shared/components/Button.module.css';

const Walkthrough = ({ history }) => (
  <Page>
    <Header/>
    <Content>
      <div className={styles.backgroundRectangleTop}></div>
      <div className={styles.backgroundRectangleBottom}></div>
      <h1>Welcome aboard Coach</h1>
      <h3>Please watch the video to proceed</h3>
      <VideoPlayer src="/videos/BG Video.mp4"/>
      <h4>Confirm that you watched the Training Video</h4>
      <button className={buttonStyles.button} onClick={() => history.push('/')}>
        Confirm
      </button>
    </Content>
  </Page>
);

export default withRouter(Walkthrough);